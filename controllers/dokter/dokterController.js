const db = require('../../config/db/db')
const helper = require('../../helper/helper')

//Include file model yang akan di gunakan
const dokterModel = require('../../models/dokter/dokterModel')


//Function untuk auto generate ID Dokter
async function generateIdDokter () {
    //kita cek ada data atau tidak
    var cekDb = await dokterModel.getAllDokter()
    //Kalau ada
    if(cekDb.length != 0)
    {
        //Ambil ID dokter, karena kita butuh hanya nomor di belakang kode dokternya. kita Slice
        var id_db = cekDb[cekDb.length-1]['id_dokter'].slice(3)
        //Karena ID berbentuk String, harus di Convert ke Integer agar bisa di tambah
        var number = parseInt(id_db)
        number++
        //Gabungkan kode DOKTER dan nomor ID baru nya
        var id_dokter = 'DTR' + number
        return id_dokter
    }
    //Jika data belum ada maka default ID dokter mulai dari 1
    else
    {
        var id_dokter = 'DTR1'
        return id_dokter
    }


}

module.exports = {

    index: async (req,res) => {
        try
        {
            //Mengambil data dokter dari model
            const data = await dokterModel.getAllDokter();

            //mengembalikan respon data dan status code berupa JSON
            return res.json({
                status: 200,
                data:data
            })
        }

        //Jika terjadi error, memberikan respon error
        catch (error)
        {
            return res.json({
                status: 500,
                message: error.message
            })
        }
    },

    show: async (req,res) => {
        try
        {
            //Mengambil parameter id di request
            const id = req.params.id

            //Mengambil data dokter dari model sesuai dengan id yang sudah di ambil tadi
            const data = await dokterModel.getDokterById(id)
    
            //Handling jika data tidak ada, jika tidak ada, maka codingan berhasil akan di skip, karena hanya 1 return yang akan di eksekusi
            if(!data)
            {
                return res.json({
                    status:404,
                    message:"Data tidak ditemukan"
                })
            }

            //Jika data ada makan codingan di atas akan di skip dan langsung masuk ke sini
            return res.json({
                status:200,
                data:data
            })
        }

        //Handling data jika error
        catch (error)
        {
            return res.json({
                status:500,
                message: error.message
            })
        }
    },

    create: async (req,res) => {
        try
        {
            //Ambil request body dan masukin ke variable
            const dataInput = req.body

            const {token} = dataInput
            const generateToken = helper.getToken()

            if(generateToken == token)
            {
                //Auto generate ID dokter menggunakan function
                var id_dokter = await generateIdDokter()

                //insert data variable tadi ke field di db
                await dokterModel.createDokter(dataInput,id_dokter)
        
                //Jika berhasil maka return respon ini
                return res.status(201).json({
                    status:201,
                    message:'Data Dokter berhasil di tambahkan'
                })
            }
            else
            {
                return res.status(401).json({
                    status:401,
                    message:'Token Invalid!'
                })
            }

        }

        //Handling error
        catch (error)
        {
            return res.json({
                status:500,
                message: error.message
            })
        }
    },

    update: async (req,res) => {
        try
        {
            const id = req.params.id
            const dataInput = req.body
            
            const data = await dokterModel.getDokterById(id)
    
            if(data)
            {
                await dokterModel.updateDokter(dataInput,id)
        
                return res.json({
                    status:201,
                    message: "Berhasil di edit!"
                })
            }
    
            return res.json({
                status:404,
                message: "Data tidak di temukan"
            })
        }
        catch (error)
        {
            return res.json({
                status:500,
                message: error.message
            })
        }
    },

    delete: async (req,res) => {
        try
        {
            const id = req.params.id
            const data = await dokterModel.getDokterById(id)
    
            if(data)
            {
                await dokterModel.deleteDokter(id)
                return res.json({
                    status:201,
                    message: "Berhasil menghapus data!"
                })
            }
    
            return res.json({
                status : 404 ,
                message: "Data tidak di temukan"
            })
        }
        catch (error)
        {
            return res.json({
                status : 500 ,
                message: error.message
            })
        }
    }

}