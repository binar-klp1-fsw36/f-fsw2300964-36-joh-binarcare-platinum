const db = require("../../config/db/db");
const helper = require('../../helper/helper')

// require model transaksi
const transaksiModel = require("../../models/transaksi/transaksiModel");

module.exports = {
  // mengambil semua data dari tabel transaksi
  getAll: async (req, res) => {
    try {
      // mengambil data melalui model
      const data = await transaksiModel.getAllTransaksi();

      res.json({
        status: 200,
        data,
      });
      // error handling
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  // mengambil data sesuai ID param dari tabel transaksi
  getById: async (req, res) => {
    try {
      // mengambil userid param dari request user
      const { userID } = req.params;

      //   mengambil data dari tabel melalui model
      const data = await transaksiModel.getTransaksiById(userID);

      if (!data) {
        res.json({
          status: 404,
          message: "Data tidak ditemukan",
        })
      } else {
      res.json({
        status: 200,
        data,
      })
    }

      // error handling
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  delById: async (req, res) => {
    try {
      const { transaksiID } = req.params;

      const data = await transaksiModel.delTransaksiById(transaksiID)

      if (!data)
      res.json({
        status: 404,
        message: "data tidak ditemukan"
      })
      res.json({
        status: 201,
        message: "data berhasil dihapus"
      })
    } catch (error) {
      return res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  postData: async (req, res) => {
    try {
      const postData = req.body;

      const {token} = postData
      const generateToken = helper.getToken()

      if(generateToken == token)
      {
        const data = await transaksiModel.postTransaksi(postData);
  
        res.json({
          status: 201,
          message: "data sucessfully posted",
        });
      }
      else
      {
        res.json({
          status: 401,
          message: "Invalid Token!",
        });
      }

    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  patchById: async (req, res) => {
    try {
      const { transaksiID } = req.params;
      const { updatedStatus } = req.body;

      const data = await transaksiModel.getTransaksiById(transaksiID)
      await transaksiModel.patchStatusTransaksi(
        transaksiID,
        updatedStatus
      );

      if (!data) {
        res.json ({
          status: 404,
          message: "Data tidak ditemukan"
        })
      }
      res.json({
        status: 201,
        data,
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  patchBuktiBayarById: async (req, res) => {
    try {
      const { transaksiID } = req.params
      const { foto } = req.body
      
      const data = await transaksiModel.patchBuktiBayar(transaksiID, foto);
      

      if (!data) {
        res.json({
          status: 404,
          message: "Data tidak ditemukan"
        })
      }
      res.json({
        status: 201,
        message: "Sucessfully uploaded"
      })
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      })
    }
  }
};
