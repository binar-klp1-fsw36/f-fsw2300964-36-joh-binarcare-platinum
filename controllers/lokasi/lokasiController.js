const db = require("../../config/db/db");
const lokasiModel = require("../../models/lokasi/lokasiModel");

module.exports = {
  getAllLokasi: async (req, res) => {
    try {
      const data = await lokasiModel.getAllLokasi();

      res.json({
        status: 200,
        data,
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  getLokasiById: async (req, res) => {
    const { id } = req.params;

    try {
      const data = await lokasiModel.getLokasiById(id);

      if (!data) {
        res.json({
          status: 404,
          message: "data tidak ditemukan",
        });
      } else
        res.json({
          status: 200,
          data,
        });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  postLokasi: async (req, res) => {
    const { nmlokasi } = req.body;

    try {
      await lokasiModel.postLokasi(nmlokasi);

      res.json({
        status: 201,
        message: `Data posted successfully`
      })
    } catch (error) {
        res.json({
            status: 500,
            message: error.message
        })
    }
  },

  deleteLokasiById: async (req, res) => {
    const { id } = req.params;

    try {
      const data = await lokasiModel.getLokasiById(id)

      if (!data) {
        res.json({
          status: 404,
          message: "data not found",
        });
      } else{ 
        await lokasiModel.deleteLokasi(id);
        res.json({
          status: 200,
          message: "successfully deleted data"
        })
      }
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  }
};
