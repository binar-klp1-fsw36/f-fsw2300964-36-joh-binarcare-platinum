const migrateUp = (req,res) => {
    const { exec } = require("child_process");

    exec("npx knex migrate:up", (err, stdout, stderr) => {
    if (err) {
        console.error();
        console.error("Error:");
        console.error(err);
        console.error();
    }
    console.log(stdout);
    console.error(stderr);
    return res.json({
        log: stdout,
        error: stderr
    })
    });

}

module.exports = migrateUp