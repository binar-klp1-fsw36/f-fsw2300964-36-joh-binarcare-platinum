const db = require('../../config/db/db')
const helper = require('../../helper/helper')

const spesialis = require('../../models/spesialis/spesialisModel')


async function generateIdSpesialis () {
    var cekDb = await spesialis.getAllSpesialis()
    if(cekDb.length != 0)
    {
        var id_db = cekDb[cekDb.length-1]['id_spesialis'].slice(3)
        var number = parseInt(id_db)
        number++
        var id_spesialis = 'SPS' + number
        return id_spesialis
    }
    else
    {
        var id_spesialis = 'SPS1'
        return id_spesialis
    }
}

module.exports = {
    index: async (req,res) => {
        try
        {
            const data = await spesialis.getAllSpesialis();
            return res.json({
                status: 200,
                data:data
            })
        }
        catch (error)
        {
            return res.json({
                status: 500,
                message: error.message
            })
        }
    },

    show: async (req,res) => {
        try
        {
            const id = req.params.id
            const data = await spesialis.getSpesialisById(id)
            if(!data)
            {
                return res.json({
                    status:404,
                    message:"Data tidak ditemukan"
                })
            }
            return res.json({
                status:200,
                data:data
            })
        }
        catch (error)
        {
            return res.json({
                status:500,
                message: error.message
            })
        }
    },

    create: async (req,res) => {
        try
        {
            const dataInput = req.body

            const {token} = dataInput
            const generateToken = helper.getToken()

            if(generateToken == token)
            {
                dataInput.id_spesialis = await generateIdSpesialis()
                await spesialis.createSpesialis(dataInput)
                return res.status(201).json({
                    status:201,
                    message:'Data Spesialis berhasil di tambahkan'
                })
            }
            else
            {
                return res.status(401).json({
                    status:401,
                    message:'Token invalid!'
                })
            }

        }
        catch (error)
        {
            return res.json({
                status:500,
                message: error.message
            })
        }
    },

    update: async (req,res) => {
        try
        {
            const id = req.params.id
            const dataInput = req.body
            const data = await spesialis.getSpesialisById(id)
            if(data)
            {
                await spesialis.updateSpesialis(dataInput,id)
                return res.json({
                    status:201,
                    message: "Berhasil di edit!"
                })
            }
    
            return res.json({
                status:404,
                message: "Data tidak di temukan"
            })
        }
        catch (error)
        {
            return res.json({
                status:500,
                message: error.message
            })
        }
    },

    delete: async (req,res) => {
        try
        {
            const id = req.params.id
            const data = await spesialis.getSpesialisById(id)
    
            if(data)
            {
                await spesialis.deleteSpesialis(id)
                return res.json({
                    status:201,
                    message: "Berhasil menghapus data!"
                })
            }
    
            return res.json({
                status : 404 ,
                message: "Data tidak di temukan"
            })
        }
        catch (error)
        {
            return res.json({
                status : 500 ,
                message: error.message
            })
        }
    }
}