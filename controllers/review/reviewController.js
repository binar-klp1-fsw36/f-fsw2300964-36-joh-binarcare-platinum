const db = require("../../config/db/db");
const reviewModel = require("../../models/review/reviewModel");

module.exports = {
  getAllReview: async (req, res) => {
    try {
      const data = await reviewModel.getAllReview();

      res.json({
        status: 200,
        data,
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  getReviewById: async (req, res) => {
    const { id_review } = req.params;
    try {
      const data = await reviewModel.getReviewById(id_review);

      if (!data) {
        res.json({
          status: 404,
          message: "Data not found",
        });
      } else {
        res.json({
          status: 200,
          data,
        });
      }
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  getReviewByUser: async (req, res) => {
    const { id_user } = req.params;
    try {
      const data = await reviewModel.getReviewByUser(id_user);

      if (!data) {
        res.json({
          status: 404,
          message: "Data not found",
        });
      } else {
        res.json({
          status: 200,
          data,
        });
      }
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  getReviewByDokter: async (req, res) => {
    const { id_dokter } = req.params;
    try {
      const data = await reviewModel.getReviewByDokter(id_dokter);

      if (!data) {
        res.json({
          status: 404,
          message: "Data not found",
        });
      } else {
        res.json({
          status: 200,
          data,
        });
      }
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  deleteReviewById: async (req, res) => {
    const { id_review } = req.params;

    try {
      const data = await reviewModel.getReviewById(id_review);
      await reviewModel.deleteReviewById(id_review);

      if (!data) {
        res.json({
          status: 404,
          message: "data not found",
        });
      } else
        res.json({
          status: 200,
          message: "successfully deleted data",
        });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  deleteReviewByUser: async (req, res) => {
    const { id_user } = req.params;

    try {
      const data = await reviewModel.getReviewByUser(id_user);
      await reviewModel.deleteReviewByUser(id_user);

      if (!data) {
        res.json({
          status: 404,
          message: "data not found",
        });
      } else
        res.json({
          status: 200,
          message: "successfully deleted data",
        });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  deleteReviewByDokter: async (req, res) => {
    const { id_dokter } = req.params;

    try {
      const data = await reviewModel.getReviewByUser(id_dokter);
      await reviewModel.deleteReviewByDokter(id_dokter);

      if (!data) {
        res.json({
          status: 404,
          message: "data not found",
        });
      } else
        res.json({
          status: 200,
          message: "successfully deleted data",
        });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  postReview: async (req, res) => {
    try {
      await reviewModel.postReview(req.body);

      res.json({
        status: 201,
        message: "Data successfully posted",
      });
    } catch (error) {
      res.json({
        status: 500,
        message: error.message,
      });
    }
  },

  patchReview: async (req, res) => {
    const { id_review } = req.params

    try {
        await reviewModel.patchReview(id_review, req.body)    

        res.json({
            status: 200,
            message: "Review change success"
        })
    } catch (error) {
        res.json({
            status: 500,
            message: error.message
        })
    }
  }
};
