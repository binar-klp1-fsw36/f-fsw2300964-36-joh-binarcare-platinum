const artikelModel = require ('../../models/artikel/artikelModel')
const helper = require('../../helper/helper')

class artikelController {
    async createArtikel(req, res) {
        try {
            const artikelData = req.body;
            const {token} = artikelData
            const generateToken = helper.getToken()

            if(generateToken == token)
            {
                const newArtikel = await artikelModel.createArtikel(artikelData);
                res.status(201).json({ message: 'Artikel created successfully' });
            }
            else{
                res.status(401).json({ message: 'Token Invalid!' });
            }

        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message });
        }
    }

    async getAllArtikel(req, res) {
        try {
            const allArtikel = await artikelModel.getAllArtikel();
            res.status(200).json(allArtikel);
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async getArtikelById(req, res) {
        try {
            const artikelId = req.params.artikelId;
            const artikelById = await artikelModel.getArtikelById(artikelId);
            if(!artikelById) {
                return res.status(404).json({ error: 'Artikel not found' });
            }
            res.status(200).json(artikelById);
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async updateArtikel(req, res) { 
        try {
            const artikelId = req.params.artikelId;
            const artikelData = req.body;
            const updatedArtikel = await artikelModel.updateArtikel(artikelId, artikelData);
            if(!updatedArtikel) {
                return res.status(404).json({ error: 'Artikel not found' });
            }
            res.status(200).json({ message: 'Artikel updated successfully' });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async deleteArtikel(req, res) {
        try {
            const artikelId = req.params.artikelId;
            const deletedArtikel = await artikelModel.deleteArtikel(artikelId);
            if(!deletedArtikel) {
                return res.status(404).json({ error: 'Artikel not found' });
            }
            res.status(200).json({ message: 'Artikel deleted successfully' });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }
}

module.exports = new artikelController();