const userModel = require('../../models/user/userModel')

class userController {

    async createUser(req, res) {
        try {
            const userData = req.body;
            await userModel.createUser(userData);
            res.status(201).json({ message: 'User created successfully' });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async getUser(req, res) {
        try {
            const allUsers = await userModel.getUser();
            res.status(200).json(allUsers);
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async getUserById(req, res) {
        try {
            const userId = req.params.userId;
            const userById = await userModel.getUserById(userId);
            if(!userById) {
                return res.status(404).json({ error: 'User not found' })};
            res.status(200).json(userById)
            }
         catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async updateUser(req, res) {
        try {
            const userId = req.params.userId;
            const userData = req.body;
            const updatedUser = await userModel.updateUser(userId, userData);
            if(!updatedUser) {
                return res.status(404).json({ error: 'User not found' })};
            res.status(200).json({ message: 'User updated successfully' });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

    async deleteUser(req, res) {
        try {
            const userId = req.params.userId;
            const deletedUser = await userModel.deleteUser(userId);
            if(!deletedUser) {
                return res.status(404).json({ error: 'User not found' })};
            res.status(200).json({ message: 'User deleted successfully' });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error', error: error.message  });
        }
    }

}

module.exports = new userController();