/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('user', (table) => {
        table.increments('user_id').primary()
        table.string('username').notNullable()
        table.string('first_name').notNullable()
        table.string('last_name').notNullable()
        table.string('email_user').notNullable()
        table.string('password').notNullable()
        table.integer('role').notNullable()
        table.string('tlp_user').notNullable()
        table.string('foto_user').notNullable()
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('user')
};