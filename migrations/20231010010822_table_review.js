/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.createTable("table_review", (table) => {
    table.string("id_review").primary();
    table.string("id_dokter").notNullable()
    table.string("id_user").notNullable();
    table.integer("rating").notNullable();
    table.string("ulasan").notNullable();
    table.string("id_transaksi").notNullable();
  })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTable("table_review")
};
