/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('artikel', (table) => {
        table.increments('artikel_id').primary()
        table.string('kategori_artikel').notNullable()
        table.string('judul_artikel').notNullable()
        table.string('isi_artikel').notNullable()
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  
};
