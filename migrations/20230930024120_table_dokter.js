/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("table_dokter", (table) => {
    table.string("id_dokter").primary();
    table.string("nama_dokter").notNullable();
    table.string("id_spesialis").notNullable();
    table.string("alamat_dokter").notNullable();
    table.string("id_lokasi").notNullable();
    table.string("tlp_dokter").notNullable();
    table.string("id_review").notNullable();
    table.string("foto_dokter").notNullable();
    table.integer("tarif_dokter").notNullable();
    table.string("jadwal_dokter").notNullable();
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("TABLE_DOKTER");
};
