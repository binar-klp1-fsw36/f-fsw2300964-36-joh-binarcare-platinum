/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.createTable('table_transaksi', (table) => {
    table.string("id_transaksi").primary()
    table.string("id_dokter",255).notNullable()
    table.bigint("jml_bayar").notNullable()
    table.string("id_user", 255).notNullable();
    table.integer("status_transaksi").notNullable();
    table.string("foto_bukti_bayar").notNullable();
  })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTable("TABLE_TRANSAKSI")
};

