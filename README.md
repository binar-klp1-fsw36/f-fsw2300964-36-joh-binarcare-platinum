# BINAR Care

This project is for Binar Academy Platinum Challenge.
Already sync with vercel

## Techs used

*   [x] **Express JS** 
*   [x] **PostgreSQL** 

## Contents

*   [Getting started](#getting-started)
    *   [Setting table](#setting-table)
    *   [Documentation](#documentation)
    
## Getting started

First make a database named **binarcare**.

Use the following code:
```
psql -d postgres -U postgres
create database binarcare
```

### Setting table
Run the following code:
```
npx knex migrate:up
```

## Documentation

* [table_dokter][table_dokter] - API Documentation
* [table_spesialis][table_spesialis] - API Documentation
* [table_lokasi][table_lokasi] - API Documentation
* [table_transaksi][table_transaksi] - API Documentation

[//]: # "Source definitions"
[table_dokter]: https://documenter.getpostman.com/view/8699100/2s9YJaYPbb "table_dokter documentation"

[table_spesialis]: https://documenter.getpostman.com/view/8699100/2s9YJaYPbb "table_spesialis documentation"

[table_lokasi]: https://documenter.getpostman.com/view/28726812/2s9YJhwKLG "table_lokasi documentation"

[table_transaksi]: https://documenter.getpostman.com/view/28726812/2s9YJc2PBW "table_transaksi documentation"
