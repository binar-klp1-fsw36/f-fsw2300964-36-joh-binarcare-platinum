const md5 = require('md5')

function getToken() {
    var periode = new Date()
    let date = ("0" + periode.getDate()).slice(-2);
    let month = ("0" + (periode.getMonth() + 1)).slice(-2);
    let year = periode.getFullYear();
    let today = year + "-" + month + "-" + date
    
    var key = "B1N4RC4R3"
    var token = md5(today + key)
    
    return token
}

module.exports = {
    getToken
}
