const express = require("express");
const router = express.Router();
const lokasiController = require("../../controllers/lokasi/lokasiController")

router.get("/", lokasiController.getAllLokasi);
router.get("/:id", lokasiController.getLokasiById);

router.post("/", lokasiController.postLokasi);
router.delete("/:id", lokasiController.deleteLokasiById)

module.exports = router;
