const express = require('express')
const userController = require('../../controllers/user/userController')

const router = express.Router()

router.get('/user', userController.getUser)
router.get('/user/:userId', userController.getUserById)
router.post('/user', userController.createUser)
router.put('/user/:userId', userController.updateUser)
router.delete('/user/:userId', userController.deleteUser)

module.exports = router