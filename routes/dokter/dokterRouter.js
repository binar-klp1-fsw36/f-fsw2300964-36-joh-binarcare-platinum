const express = require('express')
const router = express.Router()

//Import controller yang berkaitan
const dokterController = require('../../controllers/dokter/dokterController')

//Karena prefix sudah di set di file router tidak perlu di panggil lagi
router.get('/',dokterController.index)
router.get('/:id',dokterController.show)
router.post('/', dokterController.create)
router.delete('/:id', dokterController.delete)
router.put('/:id', dokterController.update)


module.exports = router