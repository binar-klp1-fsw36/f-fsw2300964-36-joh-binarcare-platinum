const router = require('express').Router()

//Memanggil file routes
const dokterRouter = require('./dokter/dokterRouter')
const spesialisRouter = require('./spesialis/spesialisRouter')
const userRouter = require ('./user/userRouter')
const artikelRouter = require ('./artikel/artikelRouter')
const transaksiRouter = require('./transaksi/transaksiRouter')
const lokasiRouter = require('./lokasi/lokasiRouter')
const reviewRouter = require('./review/reviewRouter')
const migrateUp = require('../controllers/migrateUp')
const migrateDown = require('../controllers/migrateDown')
const migrate = require('../controllers/migrate')
const storage = require('./media/storage')

//Memanggil routing pada file dokterRouter, menetapkan prefix /api/v1/dokters sehingga
//tidak perlu di panggil berkali2
router.use('/api/v1/dokters',dokterRouter)
router.use('/api/v1/spesialis',spesialisRouter)
router.use('/api/v1', userRouter)
router.use('/api/v1', artikelRouter)
router.use("/api/v1/transaksi", transaksiRouter);
router.use("/api/v1/lokasi", lokasiRouter)
router.use("/api/v1/review", reviewRouter)
router.get('/api/v1/migrateUp', migrateUp)
router.get('/api/v1/migrateDown', migrateDown)
router.get('/api/v1/migrate', migrate)
router.use('/api/v1', storage)


module.exports = router