const express = require("express");
const router = express.Router();
const transaksiController = require("../../controllers/transaksi/transaksiController");

router.get("/", transaksiController.getAll);
router.get("/:userID", transaksiController.getById);

router.delete("/:transaksiID", transaksiController.delById);

router.post("/", transaksiController.postData);

router.patch("/statustransaksi/:transaksiID", transaksiController.patchById);
router.patch("/buktibayar/:transaksiID", transaksiController.patchBuktiBayarById);

module.exports = router
// router.get("/", dokterController.index);
// router.get("/:id", dokterController.show);
// router.post("/", dokterController.create);
// router.delete("/:id", dokterController.delete);
// router.put("/:id", dokterController.update);
