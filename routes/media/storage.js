const router = require('express').Router()
const {upload} = require('../../config/storage')
const { uploadMinio } = require('../../lib/minio')

router.post('/avatar', upload.single('avatar'), async (req, res, next) => {
    const url = await uploadMinio(req.file.path, req.file.originalname)

    if(url == null){
        res.status(500).json({
            message: 'Error uploading avatar'
        })
    } else {
        res.json({
            data: {
                url: url,
            }
        })
    }
})

module.exports = router