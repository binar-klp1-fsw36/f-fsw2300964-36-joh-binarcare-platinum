const express = require('express')
const router = express.Router()

const spesialisController = require('../../controllers/spesialis/spesialisController')

router.get('/',spesialisController.index)
router.get('/:id',spesialisController.show)
router.post('/', spesialisController.create)
router.delete('/:id', spesialisController.delete)
router.put('/:id', spesialisController.update)


module.exports = router