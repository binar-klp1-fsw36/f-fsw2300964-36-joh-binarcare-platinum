const express = require("express");
const router = express.Router();
const reviewController = require("../../controllers/review/reviewController");

router.get("/", reviewController.getAllReview);
router.get("/byId/:id_review", reviewController.getReviewById);
router.get("/byUser/:id_user", reviewController.getReviewByUser);
router.get("/byDokter/:id_dokter", reviewController.getReviewByDokter);

router.delete("/byId/:id_review", reviewController.deleteReviewById);
router.delete("/byUser/:id_user", reviewController.deleteReviewByUser);
router.delete("/byDokter/:id_dokter", reviewController.deleteReviewByDokter);

router.post("/", reviewController.postReview);

router.patch("/:id_review", reviewController.patchReview)
// TINGGAL NAMBAHIN ROUTE. SEMUA CONTROLLER SM MODEL UDAH JADI

module.exports = router;
