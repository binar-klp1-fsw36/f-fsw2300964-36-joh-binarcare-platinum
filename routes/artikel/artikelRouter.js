const express = require('express')
const artikelController = require('../../controllers/artikel/artikelController')

const router = express.Router()

router.get('/artikel', artikelController.getAllArtikel);
router.get('/artikel/:artikelId', artikelController.getArtikelById);
router.post('/artikel', artikelController.createArtikel);
router.put('/artikel/:artikelId', artikelController.updateArtikel);
router.delete('/artikel/:artikelId', artikelController.deleteArtikel);

module.exports = router