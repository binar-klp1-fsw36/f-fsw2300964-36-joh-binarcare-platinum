const db = require("../../config/db/db");

const getAllTransaksi = async () => {
  return await db.select("*").from("table_transaksi");
};

const getTransaksiById = async (userid) => {
  const data = await db.select("*").from("table_transaksi").where("id_user", userid).first();
  return data
};

const delTransaksiById = async (transaksiId) => {
  return await db("table_transaksi").where("id_transaksi", transaksiId).first().del();
};

const generateidtransaksi = async () => {
  const data = await getAllTransaksi();

  if (data.length != 0) {
    const lastid = data[data.length - 1]["id_transaksi"].slice(3);

    let toNum = parseInt(lastid);
    toNum++;

    let idTransaksi = `TSI${toNum}`;

    return idTransaksi;
  } else {
    return `TSI1`;
  }
};

const postTransaksi = async (postData) => {
  const { id_dokter, jml_bayar, id_user } = postData;

  const newId = await generateidtransaksi();

  const data = await db("table_transaksi").insert(
    {
      id_transaksi: newId,
      id_dokter,
      jml_bayar,
      id_user,
      status_transaksi: 0,
      foto_bukti_bayar: "none",
    },
    ["*"]
  );

  return data;
};

const patchStatusTransaksi = async (id, updatedStatus) => {
  const data = await db("table_transaksi")
    .where({
      id_transaksi: id,
    })
    .update(
      {
        status_transaksi: updatedStatus,
      },
      ["id_transaksi", "status_transaksi"]
    );

  if (data.length === 0) {
    return "id transaksi tidak ada";
  } else return data;
};

const patchBuktiBayar = async (id, foto) => {
  return await db("table_transaksi")
    .update({
      status_transaksi: 2,
      foto_bukti_bayar: foto,
    })
    .where({ id_transaksi: id });
};

module.exports = {
  getAllTransaksi,
  getTransaksiById,
  delTransaksiById,
  postTransaksi,
  patchStatusTransaksi,
  patchBuktiBayar,
};
