const db = require("../../config/db/db");

const getAllReview = async () => {
  return await db("table_review").select("*");
};

const getReviewById = async (id_review) => {
  return await db("table_review").select("*").where({ id_review }).first();
};

const getReviewByUser = async (id_user) => {
  return await db("table_review").select("*").where({ id_user }).first();
};

const getReviewByDokter = async (id_dokter) => {
  return await db("table_review").select("*").where({ id_dokter }).first();
};

const generateIdReview = async () => {
  const data = await getAllReview();

  if (data.length != 0) {
    const lastid = data[data.length - 1]["id_review"].slice(3);

    let toNum = parseInt(lastid);
    toNum++;

    let idLokasi = `RVW${toNum}`;
    return idLokasi;
  } else return "RVW1";
};

const postReview = async (passedBody) => {
  const id_review = await generateIdReview();

  const { id_dokter, id_user, rating, ulasan, id_transaksi } = passedBody;

  await db("table_review").insert({id_review, id_dokter, id_user, rating, ulasan, id_transaksi});
};

const deleteReviewById = async (id_review) => {
    await db("table_review").where({ id_review }).del();
}

const deleteReviewByUser = async (id_user) => {
  await db("table_review").where({ id_user }).del();
};

const deleteReviewByDokter = async (id_dokter) => {
  await db("table_review").where({ id_dokter }).del();
};

const patchReview = async (id_review, passedBody) => {
    const { rating, ulasan } = passedBody

    await db("table_review").where({ id_review }).update({ rating, ulasan });
}

module.exports = {
    getAllReview,
    getReviewById,
    getReviewByDokter,
    getReviewByUser,
    postReview,
    deleteReviewByDokter,
    deleteReviewById,
    deleteReviewByUser,
    patchReview
}