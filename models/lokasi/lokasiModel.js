const db = require("../../config/db/db")

const getAllLokasi = async () => {
    return await db("table_lokasi").select("*")
}

const getLokasiById = async (id_lokasi) => {
    const data = await db("table_lokasi").select("*").where('id_lokasi', id_lokasi).first()
    return data
}

const generateIdLokasi = async () => {
    const data = await getAllLokasi()

    if (data.length != 0) {
        const lastid = data[data.length - 1]["id_lokasi"].slice(3)

        let toNum = parseInt(lastid)
        toNum++

        let idLokasi = `LKI${toNum}`
        return idLokasi
    } else return "LKI1"
}

const postLokasi = async (nmlokasi) => {
    const id_lokasi = await generateIdLokasi()

    await db("table_lokasi").insert({
        id_lokasi,
        nmlokasi
    })
}

const deleteLokasi = async (id_lokasi) => {
    await db("table_lokasi").where({id_lokasi}).del()
}

module.exports = {
    getAllLokasi,
    getLokasiById,
    postLokasi,
    deleteLokasi
}