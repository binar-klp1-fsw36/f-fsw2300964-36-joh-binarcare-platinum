const db = require("../../config/db/db")

const getAllSpesialis = async () => {
    return await db.select('*').from('table_spesialis')
}

const getSpesialisById = async (id) => {
    const data = await db.select('*').from('table_spesialis').where('id_spesialis', id).first()
    return data
}

const createSpesialis = async (data) => {
    const {id_spesialis, nm_spesialis} = data

    const result = await db('table_spesialis').insert({
        id_spesialis: id_spesialis,
        nm_spesialis: nm_spesialis
    })

    return result
}

const updateSpesialis = async (data,id) => {
    const {nm_spesialis} = data
    const result = await db('table_spesialis').where('id_spesialis',id).update({
        nm_spesialis: nm_spesialis
    })
    return result
}

const deleteSpesialis = async (id) => {
    const result = await db('table_spesialis').where('id_spesialis',id).del()
    return result
}

module.exports = {
    getAllSpesialis,
    getSpesialisById,
    createSpesialis,
    updateSpesialis,
    deleteSpesialis
}