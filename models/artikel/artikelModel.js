const db = require ("../../config/db/db")

class artikelModel {

  createArtikel(artikelData) {
    const { kategoriArtikel, judulArtikel, isiArtikel } = artikelData;
    const newArtikel = db('artikel').insert({
        kategori_artikel: kategoriArtikel,
        judul_artikel: judulArtikel,
        isi_artikel: isiArtikel });
    return newArtikel;
  }

  getAllArtikel() {
    const artikel = db('artikel').select('*');
    return artikel
  }

  getArtikelById(artikelId) {
    const artikelById = db('artikel').where('artikel_id', artikelId).first();
    return artikelById;
  }

  updateArtikel(artikelId, artikelData) {
    const { kategoriArtikel, judulArtikel, isiArtikel } = artikelData;
    const updatedArtikel = db('artikel').where('artikel_id', artikelId).update({
        kategori_artikel: kategoriArtikel,
        judul_artikel: judulArtikel,
        isi_artikel: isiArtikel});
    return updatedArtikel;
  }

  deleteArtikel(artikelId) {
    const deletedArtikel = db('artikel').where('artikel_id', artikelId).del();
    return deletedArtikel;
  }

}

module.exports = new artikelModel