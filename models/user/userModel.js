const db = require ("../../config/db/db")

class userModel {
    createUser(userData) {
        const { username, firstName, lastName, email,  password, role, telepon, foto} = userData;
        const newUser = db('user').insert({ 
          username: username,
          first_name: firstName, 
          last_name: lastName,
          email_user: email, 
          password: password,
          role: role,
          tlp_user: telepon,
          foto_user: foto });
        return newUser;
      }
    
      getUser() {
        const user = db('user').select('*');
        return user
      }
    
      getUserById(userId) {
        const userById = db('user').where('user_id', userId).first();
        return userById;
      }
    
      updateUser(userId, userData) {
        const { username, firstName, lastName, email,  password, role, telepon, foto} = userData;
        const updatedUser = db('user').where('user_id', userId).update({ 
            username: username,
            first_name: firstName, 
            last_name: lastName,
            email_user: email, 
            password: password,
            role: role,
            tlp_user: telepon,
            foto_user: foto });
        return updatedUser;
      }
    
      deleteUser(userId) {
        const deletedUser = db('user').where('user_id', userId).del();
        return deletedUser;
      }
}

module.exports = new userModel