const db = require("../../config/db/db")

const getAllDokter = async () => {
    return await db.select('*').from('table_dokter')
}

const getDokterById = async (id) => {
    return await db.select('*').from('table_dokter').where('id_dokter', id).first()
}

const createDokter = async (data,id) => {
    const {nama_dokter,id_spesialis,alamat_dokter,id_lokasi,
        tlp_dokter,id_review,foto_dokter,tarif_dokter,jadwal_dokter} = data

    const result = await db('table_dokter').insert({
        id_dokter:id,
        nama_dokter:nama_dokter,
        id_spesialis:id_spesialis,
        alamat_dokter:alamat_dokter,
        id_lokasi:id_lokasi,
        tlp_dokter:tlp_dokter,
        id_review:id_review,
        foto_dokter:foto_dokter,
        tarif_dokter:tarif_dokter,
        jadwal_dokter:jadwal_dokter
    })

    return result
}

const updateDokter = async (data,id) => {
    const {nama_dokter,id_spesialis,alamat_dokter,id_lokasi,
        tlp_dokter,id_review,foto_dokter,tarif_dokter,jadwal_dokter} = data
    const result = await db('table_dokter').where('id_dokter',id).update({
        nama_dokter:nama_dokter,
        id_spesialis:id_spesialis,
        alamat_dokter:alamat_dokter,
        id_lokasi:id_lokasi,
        tlp_dokter:tlp_dokter,
        id_review:id_review,
        foto_dokter:foto_dokter,
        tarif_dokter:tarif_dokter,
        jadwal_dokter:jadwal_dokter
    })
    return result
}

const deleteDokter = async (id) => {
    const result = await db('table_dokter').where('id_dokter',id).del()
    return result
}

module.exports = {
    getAllDokter,
    getDokterById,
    createDokter,
    updateDokter,
    deleteDokter
}