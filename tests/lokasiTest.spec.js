const lokasiController = require("../controllers/lokasi/lokasiController")
const mock = require("./testMock");

test("Should output success for GET request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await lokasiController.getAllLokasi(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output error for GET request by ID without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await lokasiController.getLokasiById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for GET request with nonexistent ID", async () => {
  const req = mock.mockRequest({},{id:"1"});
  const res = mock.mockResponse();

  await lokasiController.getLokasiById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "data tidak ditemukan",
  });
});

test("Should output not found for DELETE request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id: "1" });
  const res = mock.mockResponse();

  await lokasiController.deleteLokasiById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "data not found",
  });
});

test("Should output error for DELETE request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await lokasiController.deleteLokasiById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

// test("Should output error for POST request without request body", async () => {
//   const req = mock.mockRequest();
//   const res = mock.mockResponse();

//   await lokasiController.postLokasi(req, res);

//   expect(res.json).toHaveBeenCalledWith({
//     status: 500,
//     message: expect.any(String),
//   });
// });