const userController = require("../controllers/user/userController");
const mock = require("./testMock");

test("Should output success for GET request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await userController.getUser(req, res);

  expect(res.status).toBeCalledWith(200);
  expect(res.json).toBeCalledWith(expect.any(Object))
});

test("Should output error for GET request by nonexistent ID", async () => {
  const req = mock.mockRequest({}, {userId: 0});
  const res = mock.mockResponse();

  await userController.getUserById(req, res);

  expect(res.status).toBeCalledWith(404);
  expect(res.json).toHaveBeenCalledWith({
    error: "User not found"
  });
});

test("Should output error for GET request by non-integer ID", async () => {
  const req = mock.mockRequest({}, { userId: "user1" });
  const res = mock.mockResponse();

  await userController.getUserById(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String)
  });
});

test("Should output error for PUT request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {userId: 0});
  const res = mock.mockResponse();

  await userController.updateUser(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String),
  });
});

test("Should output error for PUT request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { userId: 0 });
  const res = mock.mockResponse();

  await userController.updateUser(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String),
  });
});

test("Should output error for PUT request with false request body and param", async () => {
  const req = mock.mockRequest({
    test: "false"
  }, { userId: 0 });
  const res = mock.mockResponse();

  await userController.updateUser(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String),
  });
});

test("Should output error for PUT request without request param and body", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await userController.updateUser(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String),
  });
});

test("Should output error for DELETE request without request param and body", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await userController.deleteUser(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String),
  });
});

test("Should output error for DELETE request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {userId: 0});
  const res = mock.mockResponse();

  await userController.deleteUser(req, res);

  expect(res.status).toBeCalledWith(404);
  expect(res.json).toHaveBeenCalledWith({
    error: 'User not found',
  });
});

test("Should output error for POST request without request body", async () => {
  const req = mock.mockRequest({});
  const res = mock.mockResponse();

  await userController.createUser(req, res);

  expect(res.status).toBeCalledWith(500);
  expect(res.json).toHaveBeenCalledWith({
    message: "Internal server error",
    error: expect.any(String),
  });
});


