const dokterController = require("../controllers/dokter/dokterController")
const mock = require("./testMock")

test("Should output success for GET request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await dokterController.index(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output not found for GET request with a nonexistend ID", async () => {
  const req = mock.mockRequest({},{
    id: 1
  });
  const res = mock.mockResponse();

  await dokterController.show(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak ditemukan",
  });
});

test("Should output not found for GET request with a nonexistend ID", async () => {
  const req = mock.mockRequest(
    {},
    {
      id: "DTR1",
    }
  );
  const res = mock.mockResponse();

  await dokterController.show(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output error for GET request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await dokterController.show(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for PUT request with a nonexistend ID", async () => {
  const req = mock.mockRequest(
    {},
    {
      id: 1,
    }
  );
  const res = mock.mockResponse();

  await dokterController.update(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak di temukan",
  });
});

test("Should output error for PUT request with no request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await dokterController.update(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for DELETE request with a nonexistend ID", async () => {
  const req = mock.mockRequest(
    {},
    {
      id: 1,
    }
  );
  const res = mock.mockResponse();

  await dokterController.delete(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak di temukan",
  });
});

test("Should output error for DELETE request with no request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await dokterController.delete(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String)
  });
});

// test("Should output error for POST request with empty request body", async () => {
//   const req = mock.mockRequest();
//   const res = mock.mockResponse();

//   await dokterController.create(req, res);

//   expect(res.json).toHaveBeenCalledWith({
//     status: 500,
//     message: expect.any(String),
//   });
// });