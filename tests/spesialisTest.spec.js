const spesialisController = require("../controllers/spesialis/spesialisController");
const mock = require("./testMock");

test("Should output success for GET request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await spesialisController.index(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output not found for GET request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {id:1});
  const res = mock.mockResponse();

  await spesialisController.show(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak ditemukan",
  });
});

test("Should output error for GET request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await spesialisController.show(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output success with data for GET request with existing ID", async () => {
  const req = mock.mockRequest({}, { id: "SPS1" });
  const res = mock.mockResponse();

  await spesialisController.show(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output not found for PATCH request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id: 1 });
  const res = mock.mockResponse();

  await spesialisController.update(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak di temukan",
  });
});

test("Should output error for PATCH request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await spesialisController.update(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output error for DELETE request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await spesialisController.delete(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for DELETE request with nonexistent num ID", async () => {
  const req = mock.mockRequest({}, {id: 1});
  const res = mock.mockResponse();

  await spesialisController.delete(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak di temukan",
  });
});

test("Should output not found for DELETE request with nonexistent string ID", async () => {
  const req = mock.mockRequest({}, { id: "SPS01" });
  const res = mock.mockResponse();

  await spesialisController.delete(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak di temukan",
  });
});

// test("Should output error for POST request without request body", async () => {
//   const req = mock.mockRequest();
//   const res = mock.mockResponse();

//   await spesialisController.create(req, res);

//   expect(res.json).toHaveBeenCalledWith({
//     status: 500,
//     message: expect.any(String),
//   });
// });