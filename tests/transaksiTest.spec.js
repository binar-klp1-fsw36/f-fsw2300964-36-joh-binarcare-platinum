const transaksiController = require("../controllers/transaksi/transaksiController");
const mock = require("./testMock");

test("Should output success for GET request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await transaksiController.getAll(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output error for GET request by ID without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await transaksiController.getById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output error for GET request by ID with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {userID: 1});
  const res = mock.mockResponse();

  await transaksiController.getById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak ditemukan",
  });
});

test("Should output error for DELETE request by ID without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await transaksiController.delById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for DELETE request by ID with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {transaksiID: 1});
  const res = mock.mockResponse();

  await transaksiController.delById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "data tidak ditemukan",
  });
});

test("Should output error for PATCH request by ID without request body", async () => {
  const req = mock.mockRequest({}, { transaksiID: 1 });
  const res = mock.mockResponse();

  await transaksiController.patchById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for PATCH request by ID with nonexistent ID", async () => {
  const req = mock.mockRequest({updatedStatus: 1}, { transaksiID: 1 });
  const res = mock.mockResponse();

  await transaksiController.patchById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak ditemukan",
  });
});

test("Should output error for PATCH bukti bayar request by ID without request body", async () => {
  const req = mock.mockRequest({}, {});
  const res = mock.mockResponse();

  await transaksiController.patchBuktiBayarById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for PATCH bukti bayar request by ID with nonexistent ID", async () => {
  const req = mock.mockRequest({foto: 1}, { transaksiID: 1 });
  const res = mock.mockResponse();

  await transaksiController.patchBuktiBayarById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data tidak ditemukan",
  });
});