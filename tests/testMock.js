const db = require("../config/db/db")

const mockRequest = (body = {}, params = {}, query = {}) => {
  return {
    body,
    params,
    query,
  };
};

const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);

  return res;
};

afterAll(() => {
  db.destroy();
});


module.exports = {
  mockRequest,
  mockResponse,
};
