const artikelController = require("../controllers/artikel/artikelController")
const mock = require("./testMock")

test("Should ouput success for GET request", async () => {
    const req = mock.mockRequest()
    const res = mock.mockResponse()

    await artikelController.getAllArtikel(req, res)

    expect(res.status).toBeCalledWith(200)
    expect(res.json).toBeTruthy()
})

test("Should ouput fail for empty POST request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await artikelController.createArtikel(req, res);

  expect(res.status).toBeCalledWith(500);
});

test("Should ouput not found for nonexistent ID request", async () => {
  const req = mock.mockRequest(
    {},
    {
      artikelId: 1
    }
  );
  const res = mock.mockResponse();

  await artikelController.getArtikelById(req, res);

  expect(res.status).toBeCalledWith(404);
});

test("Should ouput fail for empty GET by ID request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await artikelController.getArtikelById(req, res);

  expect(res.status).toBeCalledWith(500);
});

test("not found for nonexistent ID PUT request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await artikelController.updateArtikel(req, res);

  expect(res.status).toBeCalledWith(500);
});

test("Should ouput not found for nonexistent ID DELETE request", async () => {
  const req = mock.mockRequest({}, {
    artikelId: 1
  });
  const res = mock.mockResponse();

  await artikelController.deleteArtikel(req, res);

  expect(res.status).toBeCalledWith(404);
});

test("Should ouput fail for empty DELETE request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await artikelController.deleteArtikel(req, res);

  expect(res.status).toBeCalledWith(500);
});