const reviewController = require("../controllers/review/reviewController")
const mock = require("./testMock");

test("Should output success for GET request", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.getAllReview(req, res)

  expect(res.json).toHaveBeenCalledWith({
    status: 200,
    data: expect.any(Object),
  });
});

test("Should output error for GET by ID review request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.getReviewById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for GET by ID review request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {id_review: 1});
  const res = mock.mockResponse();

  await reviewController.getReviewById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data not found",
  });
});

test("Should output not found for GET by ID user request with nonexistent ID", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.getReviewByUser(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for GET by ID user request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id_user: 1 });
  const res = mock.mockResponse();

  await reviewController.getReviewByUser(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data not found",
  });
});

test("Should output not found for GET by ID dokter request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id_dokter: 1 });
  const res = mock.mockResponse();

  await reviewController.getReviewByDokter(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "Data not found",
  });
});

test("Should output not found for GET by ID dokter request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.getReviewByDokter(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for DELETE by ID request with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id_review: 1 });
  const res = mock.mockResponse();

  await reviewController.deleteReviewById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "data not found",
  });
});

test("Should output error for DELETE by ID request without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.deleteReviewById(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for DELETE by ID dokter with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id_dokter: 1 });
  const res = mock.mockResponse();

  await reviewController.deleteReviewByDokter(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "data not found",
  });
});

test("Should output error for DELETE by ID dokter without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.deleteReviewByDokter(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output not found for DELETE by ID user with nonexistent ID", async () => {
  const req = mock.mockRequest({}, { id_user: 1 });
  const res = mock.mockResponse();

  await reviewController.deleteReviewByUser(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 404,
    message: "data not found",
  });
});

test("Should output error for DELETE by ID user without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.deleteReviewByUser(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output error for PATCH by ID user without request param", async () => {
  const req = mock.mockRequest();
  const res = mock.mockResponse();

  await reviewController.patchReview(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

test("Should output error for PATCH by ID user with nonexistent ID", async () => {
  const req = mock.mockRequest({}, {id_review: 10});
  const res = mock.mockResponse();

  await reviewController.patchReview(req, res);

  expect(res.json).toHaveBeenCalledWith({
    status: 500,
    message: expect.any(String),
  });
});

// test("Should output error for POST by ID user without request body", async () => {
//   const req = mock.mockRequest();
//   const res = mock.mockResponse();

//   await reviewController.postReview(req, res);

//   expect(res.json).toHaveBeenCalledWith({
//     status: 500,
//     message: expect.any(String),
//   });
// });
