const multer = require('multer')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9)
        cb(null, uniqueSuffix + '-' + file.originalname)
    }
})

const upload = multer({
    storage: storage,
    fileFilter: function (req, file, cb) {
        console.log(file.mimetype)
        console.log("Destination Path:", storage.destination);
        console.log("Received File Object:", file);

        if (file.mimetype == 'image/jpg' || file.mimetype == 'image/png' || file.mimetype == 'image/jpeg') {
            return cb(null, true)
        } else {
            return cb(new Error('Invalid file', false))
        }
    }
})

module.exports = {
    storage, upload
}