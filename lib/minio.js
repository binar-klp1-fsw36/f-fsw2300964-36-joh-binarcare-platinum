const {minioClient, bucketName} = require('../config/minio')
const fs = require('fs')

async function uploadMinio(filePath, filename) {
    let result
    console.log(filePath)
    console.log(filename)
    
    try {
        await minioClient.fPutObject(bucketName, filename, filePath)

        result = await minioClient.presignedGetObject(bucketName, filename)

        fs.unlinkSync(filePath)

        return result
    } catch(e) {
        fs.unlinkSync(filePath)
        console.log(e.message)

        return null
    }
}

module.exports = {
    uploadMinio
}