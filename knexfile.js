// Update with your config settings.

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
module.exports = {

  development: {
    client: 'postgresql',
    connection: "postgres://default:dgeHv7ilQFm4@ep-twilight-leaf-92674412.us-east-1.postgres.vercel-storage.com:5432/verceldb?ssl=no-verify",
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: "postgres://default:1nhRQKulrx5m@ep-misty-snow-78727430.ap-southeast-1.postgres.vercel-storage.com:5432/verceldb?sslmode=require",
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
