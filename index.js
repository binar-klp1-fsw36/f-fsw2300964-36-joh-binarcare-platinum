const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors');

//Dynamic PORT, cukup ubah PORT disini jika diperlukan
const PORT = 4000

//express.json Convert Request body POST METHOD ke JSON kecuali form-data
//express.urlencoded Convert Request body POST METHOD ke JSON termasuk form-data
app.use(express.json())
app.use(express.urlencoded({ extended: false}))
app.use(cors({origin: true, credentials: true}))

const router = require('./routes/router')
app.use(router)

app.listen(PORT, () => 
{
    console.log(`Server is running on port ${PORT}`)
})